import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule }   from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginComponent } from './login/login.component';
import { AccountService } from './_services/account.service';
import { RegisterComponent } from './register/register.component';
import { AlertComponent } from './_components/alert.component';
import { ChessboardComponent } from './chessboard/chessboard.component';
import { NgxChessBoardModule } from 'ngx-chess-board';
import { NgxChessBoardService } from 'ngx-chess-board';
import { NgxChessBoardComponent } from 'ngx-chess-board';
import { ImportBoardComponent } from './import-board/import-board.component';
import { CustomChessboardComponent } from './custom-chessboard/custom-chessboard.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { HistoryGamesComponent } from './history-games/history-games.component';
import { GameComponent } from './game/game.component';
import { TutoringComponent } from './tutoring/tutoring.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AlertComponent,
    ChessboardComponent,
    ImportBoardComponent,
    CustomChessboardComponent,
    HistoryGamesComponent,
    GameComponent,
    TutoringComponent
  ],
  imports: [
    NgxChessBoardModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    NgbModule,
    FontAwesomeModule,
    HttpClientModule,
    ColorPickerModule
  ],
  providers: [AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }

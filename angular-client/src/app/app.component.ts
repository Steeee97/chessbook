import { Component, OnInit, ViewChild } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from './_models';
import { AccountService } from './_services';
import { faInbox } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Tutor } from './_models/tutor';
import { AlertService } from './_services/alert.service';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, map, Observable, OperatorFunction } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'AngularClient';
  faInbox = faInbox;
  user:User | null = null;
 

  constructor(
    private accountService:AccountService, 
    ){
      this.accountService.user.subscribe(x => this.user = x);
      
  }

  ngOnInit(){
  }

  logged(){
    return this.accountService.logged();
  }


  logout() {
    this.accountService.logout();
  } 
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faPerson } from '@fortawesome/free-solid-svg-icons';
import { User } from '../_models';
import { AccountService } from '../_services';
import { faChessBoard, faPalette, faCheck, faChess, faArchive, faHandshakeAngle, faPersonChalkboard } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: '',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent{
  faChessBoard = faChessBoard;
  faChess =  faChess;
  faCheck = faCheck;
  faPalette = faPalette;
  faArchive = faArchive;
  faPersonChalkboard = faPersonChalkboard;
  user:User | null = null;
  constructor(private router: Router, private accountService: AccountService) { 
    this.accountService.user.subscribe(x => this.user = x);
  }


}

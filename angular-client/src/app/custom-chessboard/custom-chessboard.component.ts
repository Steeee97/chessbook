import { ViewChild, Component, OnInit } from '@angular/core';
import { NgxChessBoardComponent, NgxChessBoardService, PieceIconInput } from 'ngx-chess-board'
import { first } from 'rxjs';
import { faSave, faRotateBack, faCancel, faCircleNotch, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomChessboard } from '../_models/custom-board';
import { CustomBoardService } from '../_services/custom-board.service';
import { AccountService } from '../_services';
import { User } from '../_models';
import { AlertService } from '../_services/alert.service';
import { FuncsService } from '../_services/funcs.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-custom-chessboard',
  templateUrl: './custom-chessboard.component.html',
  styleUrls: ['./custom-chessboard.component.css']
})
export class CustomChessboardComponent implements OnInit{

  @ViewChild('board', {static: false}) board: NgxChessBoardComponent;
  faSave = faSave;
  faRotateBack = faRotateBack;
  faCancel = faCancel;
  faCircleNotch = faCircleNotch;
  faChevronLeft = faChevronLeft;
  loading:boolean = false;
  pieceIconInput: PieceIconInput;
  customBoard: CustomChessboard = new CustomChessboard();
  user:User | null = null;
  closeResult: string= "";

  @ViewChild('defaultModal') defaultModal : any;
  @ViewChild('cancelModal') cancelModal : any;

  constructor(private ngxChessBoardService: NgxChessBoardService, 
    private config: NgbModalConfig,
    private modalService: NgbModal, 
    private customBoardService: CustomBoardService,
    private accountService: AccountService,
    private alertService: AlertService,
    private funcs: FuncsService,
    private router: Router
    ){  }

   cancel(){
    this.loading = true;
     let id = this.customBoard.userId;

    this.customBoardService.get(id)
    .pipe(first())
    .subscribe({
      next: (resp) => {
        this.customBoard = resp;
        this.customBoard.userId = id
        this.close();
        this.loading = false;
      },
      error: error => {
        this.alertService.error(error.error);
        this.loading = false;
      }
    });
   }

   save(){
    this.loading = true;
    this.customBoardService.create(this.customBoard)
    .subscribe({
      next: () => {
        this.alertService.success("Salvataggio avvenuto correttamente!");
        this.loading = false;
      },
      error: error => {
        this.alertService.error("Errore durante il salvataggio!");
        this.loading = false;
      }
    });
   }

   default(){
    this.loading = true;

    let cust = new CustomChessboard();
    cust.default();
    cust.id = this.customBoard.id;
    cust.userId = this.customBoard.userId;

    this.customBoardService.create(cust)
    .subscribe({
      next: () => {
        this.loading = false;
        this.customBoard = cust;
        this.close();
      },
      error: error => {
        this.alertService.error("Errore durante il salvataggio della configurazione!");
        this.loading = false;
      }
    });
   }


  openModalDefault(){
    this.modalService.open(this.defaultModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
    });
  }

  openModalCancel(){
    this.modalService.open(this.cancelModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
    });
  }

  close(){
      this.modalService.dismissAll();
  }

    /* 
    *   Gestione Modal Generico
    */
  
    open(content:any) {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
    }

  
    ngOnInit(): void {

      this.loading = true;
      this.accountService.user.subscribe(x => this.user = x);
      let id = "";
  
      if(this.user != null){
        id = this.user.id;
      
        this.customBoardService.get(id)
        .pipe(first())
        .subscribe({
          next: (resp) => {
            this.customBoard = resp;
            this.customBoard.userId = id
          },
          error: error => {
            this.router.navigate(["/"]);
            this.alertService.error(error.error);
          }
        });
  
        this.config.backdrop = 'static';
        this.config.keyboard = false;
      
        this.pieceIconInput =this.funcs.getPieceIconInput();
        
        this.loading = false;

      }else{
         this.router.navigate(["/"]);
      }

    }

    back(){
      this.router.navigate(['/']);  
  }

}

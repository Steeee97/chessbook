﻿export class simpleUser{
    id: string;
    role: userRole;
    username: string;


    constructor(){
        this.id = "";
        this.role = userRole.User;
        this.username = "";
    }
}

export class User extends simpleUser{
    password: string = "";
    token: string = "";

}

export enum userRole {
    Admin,
    User,
    Manager
}
export class CustomChessboard{
    id: string;
    userId: string;
    lightColor: string;
    darkColor: string;
    showCoords:boolean;
    showLastMove:boolean;
    showLegalMoves:boolean;
    showActivePiece: boolean;
    sourcePointColor: string;
    destinationPointColor: string;
    showPossibleCaptures: boolean;

    constructor(){
        this.id = "";
        this.userId = "";
        this.lightColor = "";
        this.darkColor = "";
        this.showCoords = false;
        this.showLegalMoves = true;
        this.showActivePiece = true;
        this.sourcePointColor = "";
        this.destinationPointColor = "";
        this.showPossibleCaptures = true;
        
    }

    default(){
        this.darkColor = "rgba(119,150,86,255)";
        this.lightColor = "rgba(203,201,190,255)";
        this.showCoords = true;
        this.showLastMove = true;
        this.showLegalMoves = true;
        this.showActivePiece = true;
        this.sourcePointColor = "rgba(169,142,80,255)";
        this.destinationPointColor = "rgba(178,142,27,255)";
        this.showPossibleCaptures = true;
    }
}
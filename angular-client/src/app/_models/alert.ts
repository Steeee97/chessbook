export class Alert {
    id: string = "";
    type: AlertType = 0;
    message: string = "";
    autoClose: boolean = false;
    keepAfterRouteChange: boolean = false;
    fade: boolean = false;
    pBarValue: number = 0;

    constructor(init?:Partial<Alert>) {
        Object.assign(this, init);
    }
}

export enum AlertType {
    Success,
    Error,
    Warning,
    Info
    
}

export class AlertModel extends Alert{
}
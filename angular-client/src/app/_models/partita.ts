export class simplePartita{
   _id: ID; // opzionale
   date: string;
   id_user:string;
   name: string;
   description: string;
}

export class ID {
   $oid: string;
}

export class Partita extends simplePartita{
    moves: Array<Move>;
}
 export class Move{
    id_move: string; // opzionale
    number: string; // numero mossa
    move: string; // la mossa
    color: string; // "bianco" o "nero" forse tutto maiuscolo
    comments: Array<Comment>;
 }

 export class Comment{
    id_comment: string; // opzionale
    id_user: string;
    date: string;
    comment: string;
 }
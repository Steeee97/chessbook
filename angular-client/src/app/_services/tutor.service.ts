﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from 'src/app/_models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CustomChessboard } from '../_models/custom-board';
import { Tutor } from '../_models/tutor';
import { Tutoring } from '../_models/tutoring';

type UserNullable<User> = User | null;

@Injectable({ providedIn: 'root' })
export class TutorService {
    tutor:Tutoring;
    
    constructor(
        private router: Router,
        private http: HttpClient,
    ){
        
    }

    set(t:Tutoring) {
        return this.http.post('http://localhost:5004/api/tutor/create', t);
    }

    get(tutorId:string){
        return this.http.get('http://localhost:5004/api/tutor/'+tutorId)
        .pipe(map(resp => {
            let tutors:Tutoring[] = (<Tutoring[]> resp);
            return tutors;
            
        }));;
    }

    delete(GameId:string) {
        return this.http.delete('http://localhost:5004/api/tutor/'+GameId);
    }
    

    

}
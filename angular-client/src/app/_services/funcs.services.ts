import { Injectable } from '@angular/core';
import { PieceIconInput } from 'ngx-chess-board';

@Injectable({
  providedIn: 'root'
})

export class FuncsService {
  constructor() { }
  
  getPieceIconInput(): PieceIconInput{
    let pieceTemp: PieceIconInput = {
        blackBishopUrl : 'assets/scacchiera/chess-bishop-solid.svg',
        blackKingUrl : "assets/scacchiera/chess-king-solid.svg",
        blackKnightUrl : "assets/scacchiera/chess-knight-solid.svg",
        blackPawnUrl : "assets/scacchiera/chess-pawn-solid.svg",
        blackQueenUrl : "assets/scacchiera/chess-queen-solid.svg",
        blackRookUrl : "assets/scacchiera/chess-rook-solid.svg",
        whiteBishopUrl : 'assets/scacchiera/chess-bishop-solid-white.svg',
        whiteKingUrl : "assets/scacchiera/chess-king-solid-white.svg",
        whiteKnightUrl : "assets/scacchiera/chess-knight-solid-white.svg",
        whitePawnUrl : "assets/scacchiera/chess-pawn-solid-white.svg",
        whiteQueenUrl : "assets/scacchiera/chess-queen-solid-white.svg",
        whiteRookUrl : "assets/scacchiera/chess-rook-solid-white.svg"
      };

    return pieceTemp;
  }

  
}
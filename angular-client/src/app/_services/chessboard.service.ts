﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from 'src/app/_models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CustomChessboard } from '../_models/custom-board';
import { ID, Partita } from '../_models/partita';

type UserNullable<User> = User | null;

@Injectable({ providedIn: 'root' })
export class ChessboardService {
    partita:Partita;
    
    constructor(
        private router: Router,
        private http: HttpClient,
    ){
        
    }

    get(userId:string){
        return this.http.get<Partita[]>('http://localhost:5003/matches/'+userId)
        .pipe(map(resp => {
            let part = (<Partita[]>resp);
            return part;
        }));;
    }

    getId(Id:string){
        return this.http.get<Partita>('http://localhost:5003/match/'+Id)
        .pipe(map(resp => {
            let part = (<Partita>resp);
            return part;
        }));;
    }

    // aggiunta ora
    create(cust:Partita, arrayMoves:string[]) {
        let body = {
            "date": cust.date,
            "id_user": cust.id_user,
            "name": cust.name,
            "description": cust.description,
            "moves": arrayMoves
            // "moves": ["1. b4 b5", "2. c4 c5 ", " 3. d4 d5 ", "4. cxd5 Qxd5 ", " 5. bxc5 Ba6", "7. c8=N Ne5"] //cust.moves
        }
        return this.http.post<Partita>('http://localhost:5003/match', body)
        .pipe(map(resp => {
            let part = (<Partita>resp);
            return part;
        }));
    }
   
    createComment(id_game:string, id_move:string, id_user:string, date:string, comment:string) {
        let body = {
            "date": date,
            "id_user": id_user,
            "id_move": id_move,
            "comment": comment
            // "moves": ["1. b4 b5", "2. c4 c5 ", " 3. d4 d5 ", "4. cxd5 Qxd5 ", " 5. bxc5 Ba6", "7. c8=N Ne5"] //cust.moves
        }
        return this.http.post('http://localhost:5003/match/'+id_game, body)
    }
    //nel body deve esserci id move, id user, comment
   update(id_game:string, id_mossa:string, id_utente:string, commento:string) {
        let body = {
            "id_move" : id_mossa,
            "id_user": id_utente,
            "comment": commento,
        }
        // capiamo se sta roba e' corretta
        return this.http.put('http://localhost:5003/match/'+id_game, body);
    } 

    delete(userId:string){
        return this.http.delete('http://localhost:5003/match/'+userId);
    }

}
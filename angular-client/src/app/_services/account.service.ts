﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from 'src/app/_models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Tutor } from '../_models/tutor';

type UserNullable<User> = User | null;

@Injectable({ providedIn: 'root' })
export class AccountService {

    private userSubject: BehaviorSubject<User | null>;
    public user: Observable<User | null>;
    usr:User | null = null;

    constructor(
        private router: Router,
        private http: HttpClient,
        private jwtHelper: JwtHelperService
    ){

        this.userSubject = new BehaviorSubject<User | null>(JSON.parse(localStorage.getItem('user') || "{}"));
        this.user = this.userSubject.asObservable() || null;
        
    }

    logged():boolean{
        this.usr = this.userValue;

        if(this.usr){
            if(this.usr?.token && !this.jwtHelper.isTokenExpired(this.usr?.token)){
            return true;
            }
        }
        return false;
    }

    public get userValue(): User | null {
        return this.userSubject.value;
    }

    login(username:string, password:string) {
        const login = {'username': username, 'password': password};
        return this.http.post<User>('http://localhost:5001/api/auth/login', login)
            .pipe(map(resp => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                const user = (<User>resp);
                localStorage.setItem('user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/login']);
    }

    register(email:string, username:string, password:string) {
        const register = {'email': email, 'username': username, 'password': password};
        return this.http.post('http://localhost:5001/api/auth/register', register);
    }

    account(){
        return this.http.get<Tutor[]>('http://localhost:5001/api/account');
    }
}

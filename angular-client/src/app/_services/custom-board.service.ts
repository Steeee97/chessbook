﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from 'src/app/_models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CustomChessboard } from '../_models/custom-board';

type UserNullable<User> = User | null;

@Injectable({ providedIn: 'root' })
export class CustomBoardService {
    customBoard:CustomChessboard;
    
    constructor(
        private router: Router,
        private http: HttpClient,
    ){
        
    }

    create(cust:CustomChessboard) {
        return this.http.post('http://localhost:5002/api/custChessboard/create', cust);
    }

    get(userId:string){
        return this.http.get<CustomChessboard>('http://localhost:5002/api/custChessboard/'+userId)
        .pipe(map(resp => {
            let cust = (<CustomChessboard>resp);
            if(cust == null){
                cust = new CustomChessboard();
                cust.default();
            }
            return cust;
        }));;
    }

    

}
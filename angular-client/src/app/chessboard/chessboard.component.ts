import { ViewChild, ChangeDetectionStrategy, Component, EventEmitter, Output, OnInit } from '@angular/core';
import {
  MoveChange,
  NgxChessBoardComponent,
  NgxChessBoardService,
  NgxChessBoardView,
  PieceIconInput
} from 'ngx-chess-board'
import { BehaviorSubject, first } from 'rxjs';
import { faBan, faChessPawn, faUndo, faCircleNotch, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { CustomBoardService } from '../_services/custom-board.service';
import { AccountService } from '../_services';
import { AlertService } from '../_services/alert.service';
import { FuncsService } from '../_services/funcs.services';
import { Router } from '@angular/router';
import { User } from '../_models';
import { CustomChessboard } from '../_models/custom-board';

@Component({
  selector: 'chessboard',
  templateUrl: './chessboard.component.html',
  styleUrls: ['./chessboard.component.css'],
  //changeDetection: ChangeDetectionStrategy.OnPush, ??????
})
export class ChessboardComponent implements OnInit {
  @ViewChild('board', {static: false}) board: NgxChessBoardComponent;
  faBan = faBan;
  faUndo = faUndo;
  faChessPawn = faChessPawn;
  faCircleNotch = faCircleNotch;
  faChevronLeft = faChevronLeft;
  isWhite:boolean = true;
  closeResult: string= "";
  pieceIconInput: PieceIconInput;
  loading:boolean = false;
  user:User | null = null;
  customBoard: CustomChessboard = new CustomChessboard();

  @ViewChild('resetModal') resetModal : any;

  constructor(
    private ngxChessBoardService: NgxChessBoardService, 
    private config: NgbModalConfig,
    private modalService: NgbModal, 
    private customBoardService: CustomBoardService,
    private accountService: AccountService,
    private alertService: AlertService,
    private funcs: FuncsService,
    private router: Router
    ) {
   }

  public pgn: string = '';
  public temppgn: string = '';
  public whiteMoves: string [] = [];
  public blackMoves: string [] = [];
  public whiteToMove: boolean = true;
  public whiteMoveCount = 0;
  public blackMoveCount = 0;
  public turn = 0;
  public arrayColors: any = {
    color1: '#2883e9',
    color2: '#e920e9',
    color3: 'rgb(255,245,0)',
    color4: 'rgb(236,64,64)',
    color5: 'rgba(45,208,45,1)'
  };

  public selectedColor: string = 'color1';


  public reverse(): void {
      this.isWhite = !this.isWhite;
      this.board.reverse();
    }
  
public beginning(): void {
    this.temppgn = '';
    this.board.reset();
    this.turn = 0;
  }

  public moveCallback(move: MoveChange): void {
        //this.fen = this.boardManager.getFEN();
        //ottengo il PGN della partita
        this.pgn = this.board.getPGN();
        var temp = this.pgn.split(' ');
        // Se temp.length - 2 termina con un punto, e' una mossa del bianco, altrimenti del nero
        // es: 1. e4 e5
        if (temp[temp.length-2].endsWith('.')) {
          this.whiteMoves.push(temp[temp.length-1]);
          this.whiteToMove = false;
        } else {
          this.blackMoves.push(temp[temp.length-1]);
          this.whiteToMove = true;
        }

        console.log(move);
    }

  
  // go to end da implementare

  public undo(): void {
        var temp = this.pgn.split(' ');
        //mossa del bianco
        if (temp[temp.length-2].endsWith('.')) {
          this.pgn = this.pgn.substring(0, this.pgn.lastIndexOf(temp[temp.length-2]));
          if (this.pgn == '') {
            this.beginning();
          } else {
            this.board.setPGN(this.pgn);
          }
        } else {
        this.pgn = this.pgn.substring(0, this.pgn.lastIndexOf(temp[temp.length-1]))
        this.board.setPGN(this.pgn);
      } 
        //this.board.undo();
        console.log(this.pgn)
    }


    openModalReset(){
      this.modalService.open(this.resetModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
      });
  }
  
    reset(): void {
      this.board.reset();
      this.whiteMoves = [];
      this.blackMoves = [];
      this.pgn = '';
      this.temppgn = '';
      this.modalService.dismissAll();
  }
  
  closeReset(){
      this.modalService.dismissAll();
  
  }
  
  
  
    /* 
    *   Gestione Modal Generico
    */
  
    open(content:any) {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
    }
  
  ngOnInit(): void {
    this.loading = true;
    this.accountService.user.subscribe(x => this.user = x);
    let id = "";

    if(this.user != null){
      id = this.user.id;
    
      this.customBoardService.get(id)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.customBoard = resp;
          this.customBoard.userId = id
        },
        error: error => {
          this.router.navigate(["/"]);
          this.alertService.error(error.error);
        }
      });

      this.config.backdrop = 'static';
      this.config.keyboard = false;
    
      this.pieceIconInput =this.funcs.getPieceIconInput();
      this.loading = false;

    }else{
       this.router.navigate(["/"]);
    }

  }

  back(){
    this.router.navigate(['/']);  
  }
  @Output() public restart = new EventEmitter<void>();
}

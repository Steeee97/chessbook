import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountService } from '../_services';
import { first } from 'rxjs';
import { User } from '../_models';
import { faSpinner} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faSpinner = faSpinner;
  invalidLogin: boolean = false;
  loading: boolean = false;
  username: string = "";
  password: string = "";
  user: User | null = null;
  constructor(private router: Router, private accountService: AccountService) { }

  ngOnInit(): void {
    if(this.accountService.logged()) 
      this.router.navigate(["/"]);
  }


  login(){
    this.loading = true;
    this.invalidLogin = false;

    const credential = {
      'username': this.username,
      'password': this.password
    }

    this.accountService.login(this.username, this.password)
            .pipe(first())
            .subscribe({
                next: () => {

                    this.router.navigate(["/"]);
                },
                error: error => {
                  this.invalidLogin = true;
                  this.loading = false;
                }
            });
  }
}

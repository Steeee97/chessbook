import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSpinner} from '@fortawesome/free-solid-svg-icons';
import { concat, ObservableInput } from 'rxjs';
import { AccountService } from '../_services';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  faSpinner = faSpinner;
  err:string[] = [];
  invalid:boolean = false;
  loading:boolean = false;
  username:string = "";
  email:string = "";
  password:string = "";
  passwordConf: string = "";

  constructor(private router: Router, private accountService: AccountService, private alertService: AlertService) { }

  ngOnInit(): void {
    if(this.accountService.logged()) 
    this.router.navigate(["/"]);
  }

  register(){
    this.loading = true;
    this.invalid = false;
    this.err = [];
    
    if(this.password == this.passwordConf){

      this.accountService.register(this.email, this.username, this.password)
      .subscribe({
          next: () => {
              this.router.navigate(["/login"]);
              this.alertService.success("Registrazione avvenuta con successo, accedi per usare la web app!");
          },
          error: error => {
            this.err = error.error;
            let stringError: string ="";
            for(let i=0; i <5; i++){
              if(error.error[i] != null){
                stringError += i+1 + ") " + error.error[i]+ " ";
              }
            }
            this.alertService.error(stringError);
            this.loading = false;
          }
      });
    }else{
      this.alertService.warn("Le password non corrispondono");
      this.loading = false;
    }

  }

}

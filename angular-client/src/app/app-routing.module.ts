import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { JwtGuard } from './_settings/jwt.guard';
import { LoginComponent } from './login/login.component';
import { JwtModule } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { User } from './_models';
import { RegisterComponent } from './register/register.component';
import { ChessboardComponent } from './chessboard/chessboard.component';
import { ImportBoardComponent } from './import-board/import-board.component';
import { CustomChessboardComponent } from './custom-chessboard/custom-chessboard.component';
import { HistoryGamesComponent } from './history-games/history-games.component';
import { GameComponent } from './game/game.component';
import { TutoringComponent } from './tutoring/tutoring.component';


export function tokenGetter(){
  let token = "";

  if(localStorage.getItem('user')){
    let userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user') || "{}"));
    const user = userSubject.value;
    token = user.token;
  }

  return token;
}

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [JwtGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'chessboard', component: ChessboardComponent, canActivate: [JwtGuard]},
  {path: 'importboard', component: ImportBoardComponent, canActivate: [JwtGuard]},
  {path: 'customboard', component: CustomChessboardComponent, canActivate: [JwtGuard]},
  {path: 'history', component: HistoryGamesComponent, canActivate: [JwtGuard]},
  {path: 'history/:id', component: GameComponent, canActivate: [JwtGuard] },
  {path: 'tutoring', component: TutoringComponent, canActivate: [JwtGuard]},

  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:5001"],
        disallowedRoutes: []
      }
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

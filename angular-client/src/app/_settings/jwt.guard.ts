import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { User } from '../_models';
import { AccountService } from '../_services';

@Injectable({
  providedIn: 'root'
})
export class JwtGuard implements CanActivate {
  user:User | null;
  constructor(private router: Router, private jwtHelper: JwtHelperService, private accountService: AccountService) { 
    this.user = null;

  }
  
  canActivate(){
    this.accountService.user.subscribe(x => this.user = x);
    const token = this.user?.token || "";

    if(token && !this.jwtHelper.isTokenExpired(token)){
      return true;
    }

    this.router.navigate(['login']);
    return false;
    
  }
  
}

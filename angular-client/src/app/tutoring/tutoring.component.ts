import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faCircleNotch, faChevronLeft, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs';
import { Partita } from '../_models/partita';
import { Tutoring } from '../_models/tutoring';
import { User } from '../_models/user';
import { AccountService } from '../_services';
import { AlertService } from '../_services/alert.service';
import { ChessboardService } from '../_services/chessboard.service';
import { TutorService } from '../_services/tutor.service';


@Component({
  selector: 'app-tutoring',
  templateUrl: './tutoring.component.html',
  styleUrls: ['./tutoring.component.css']
})
export class TutoringComponent implements OnInit {
  @ViewChild('deleteModal') deleteModal : any;
  faCircleNotch = faCircleNotch;
  faChevronLeft = faChevronLeft;
  faTrash = faTrash;
  loading: boolean = false;
  user:User | null = null;
  closeResult: string= "";
  deleteId:string;
  
  page = 1;
  pageSize = 10;
  collectionSize= 1;
  partite: Partita[] = [];
  tutorings: Array<Tutoring>;

  constructor(
    private router: Router,
    private alertService: AlertService,
    private chessboardService: ChessboardService,
    private tutorService: TutorService,
    private accountService: AccountService,
    public activatedRoute: ActivatedRoute,
    private modalService: NgbModal, 
  ) { }

  ngOnInit(): void {
    this.get();
  }

  get(){
    this.partite = [];
    this.loading = true;

    this.accountService.user.subscribe(x => this.user = x); // prende l'id dell'utente attuale
    let id = "";

    if(this.user != null){
      id = this.user.id;


      //lettura partite
      this.tutorService.get(id)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.tutorings = resp;

          if(this.tutorings && this.tutorings.length > 0){
            for(let i=0; i < this.tutorings.length; i++){
              this.chessboardService.getId(this.tutorings[i].gameId)
              .pipe(first())
              .subscribe({
                next: (resp) => {
                this.partite.push(resp);

                
              },
              error: error => {
                this.alertService.error(error.error);
                this.loading = false;
              }
              });
            }
          }
        },
        error: error => {
          this.alertService.error(error.error);
        }
      });

      

      if(this.partite != null && this.partite){
            this.collectionSize = this.partite.length;
            this.refreshPartite();
      }
      
      this.loading = false;
    }else{
      this.router.navigate(["/"]);
    }
  }

  refreshPartite() {
    this.partite
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  back(){
    this.router.navigate(['/']);  
  }

  openGame(id:string){
    this.router.navigate(['/history/', id], {relativeTo: this.activatedRoute});
  }

  clickStopper(event:any) {
    event.stopPropagation();
    return;
  }

  openModalDelete(id:string){
    this.deleteId = id;
    this.modalService.open(this.deleteModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
    });
  }

  close(){
      this.deleteId = "";
      this.modalService.dismissAll();
  }

  delete(){
    console.log(this.deleteId);
    this.tutorService.delete(this.deleteId)
    .subscribe({
      next: (resp) => {
        this.alertService.success("Partita eliminata con successo!");
        this.close();
        this.get();
      },
      error: error => {
        this.alertService.error(error.error);
        this.close();
      }
    });
  }

  /* 
  *   Gestione Modal Generico
  */
  
  open(content:any) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}


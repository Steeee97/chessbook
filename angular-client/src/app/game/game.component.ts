import { AfterViewChecked, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faBackspace } from '@fortawesome/free-solid-svg-icons';
import { first, Observable, Subscription, interval, throwIfEmpty, OperatorFunction, debounceTime, distinctUntilChanged, map } from 'rxjs';
import { Comment, Move, Partita } from '../_models/partita';
import { AlertService } from '../_services/alert.service';
import { ChessboardService } from '../_services/chessboard.service';
import { AccountService } from '../_services';
import { User } from '../_models';
import { CustomBoardService } from '../_services/custom-board.service';
import { CustomChessboard } from '../_models/custom-board';
import { FuncsService } from '../_services/funcs.services';
import { faBan, faChessPawn, faUndo, faForwardStep, faBackwardStep, faCircleNotch, faChevronLeft, faPersonChalkboard, faRotateRight } from '@fortawesome/free-solid-svg-icons';
import { MoveChange, NgxChessBoardComponent, PieceIconInput } from 'ngx-chess-board';
import { Tutor } from '../_models/tutor';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TutorService } from '../_services/tutor.service';
import { Tutoring } from '../_models/tutoring';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  @ViewChild('board', {static: false}) board: NgxChessBoardComponent;
  @ViewChild('scroll') private myScrollContainer: ElementRef;
  faBan = faBan;
  faUndo = faUndo;
  faChessPawn = faChessPawn;
  faCircleNotch = faCircleNotch;
  faChevronLeft = faChevronLeft;
  faForwardStep = faForwardStep;
  faBackwardStep = faBackwardStep;
  faPersonChalkboard = faPersonChalkboard;
  faRotateRight = faRotateRight;
  isWhite:boolean = true;
  user:User | null = null;
  id:string;
  partita:Partita;
  loading:boolean;
  customBoard: CustomChessboard = new CustomChessboard();
  pieceIconInput: PieceIconInput;
  pgnSetted: boolean= false;
  userid: string = "";
  sub:Subscription;


  public game: Partita;
  public pgn: string = '';
  public temppgn: string = '';
  public whiteMoves: string [] = [];
  public blackMoves: string [] = [];
  public whiteToMove: boolean = true;
  public turn = 0;
  public trueturn = 0;
  public move = 0;
  public commentCount = -1;
  public comments: any[] = [];
  public lastComment: string = '';
  public visComment : string = '';
  moves : Array<Move> = [];

  constructor(
    private router: Router,
    public activatedRoute: ActivatedRoute,
    private chessboardService: ChessboardService,
    private customBoardService: CustomBoardService,
    private alertService: AlertService,
    private accountService: AccountService,
    private funcs: FuncsService,
    private modalService: NgbModal,
    private tutorService: TutorService
  ) { }


  ngOnInit(): void {
    this.loading = true;
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];         
    });

    this.accountService.user.subscribe(x => this.user = x); // prende l'id dell'utente attuale
    if(this.user != null){
      this.userid = this.user.id;
    };

    if(this.id != ""){
      this.chessboardService.getId(this.id)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.partita = resp;
          this.game = this.partita;
          this.getMoves();

          this.loading = false;
        },
        error: error => {
          this.back();
          this.alertService.error(error.error);
          this.loading = false;
        }
      });

      this.customBoardService.get(this.userid)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.customBoard = resp;
          this.customBoard.userId = this.id;
        },
        error: error => {
          this.router.navigate(["/"]);
          this.alertService.error(error.error);
        }
      });
    
      this.pieceIconInput =this.funcs.getPieceIconInput();
      this.scrollToBottom();
      this.loading = false;

    }else{
      this.back();
      this.alertService.error("Errore interno, id non trovato!");
    }

  //this.getMoves();
  /*this.sub = interval(1000)
    .subscribe((val) => { 
       });*/
  }

  update(){
    this.updateMovesComment();
  }

  scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }

  updateMovesComment(){
    this.chessboardService.getId(this.id)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.partita = resp;
          this.game = this.partita;
          this.getMoves();       
        },
        error: error => {
          this.back();
          this.alertService.error(error.error);
        }
      });
  }

  public saveComment() {
    // Prendo la mossa, gli salvo un commento dentro
    let x = new Comment;
    x.comment = this.lastComment;
    this.accountService.user.subscribe(x => this.user = x); // prendo l'id dell'utente
    if (this.user != null)
      x.id_user = this.user?.id;
    let date = new Date;
    x.date = date.toLocaleDateString();
      this.moves[this.move].comments.push(x);
      this.chessboardService.createComment(this.game._id.$oid, this.moves[this.commentCount].id_move, x.id_user, x.date, x.comment)
    .subscribe({
      next: () => {
        this.loading = false;
        this.alertService.success("Commento salvato correttamente!");
        this.lastComment = "";
        this.updateMovesComment();
      },
      error: error => {
        this.alertService.error("Errore durante il salvataggio!");
        this.loading = false;
      }
    });
  }
  // ottengo le mosse e i commenti associati
  // Iniziamo a salvare solo il primo commento
  public getMoves() {
    //console.log(this.game);
    this.moves = [];
    this.whiteMoves = [];
    this.blackMoves = [];
    let whitePad = true;
    let moveArray = this.game.moves;
    //console.log(this.game.moves)
    for (let i = 0; i < moveArray.length; i++) {
      if (moveArray[i].color == 'WHITE') {
        this.whiteMoves.push(moveArray[i].move);
        whitePad = false;
      } else {
        this.blackMoves.push(moveArray[i].move);
        whitePad = true;
      }
      this.moves.push(moveArray[i]);
    }
    //console.log(this.whiteMoves);
    this.trueturn = this.whiteMoves.length + this.blackMoves.length;
    //console.log(this.whiteMoves)
  }

   public reverse(): void {
    this.isWhite = !this.isWhite;
    this.board.reverse();
  }

  back(){
    if(this.userid == this.partita.id_user){
      this.router.navigate(['/history']);  
    }else{
      this.router.navigate(['/tutoring']);
    }
  }

  public moveCallback(move: MoveChange): void {
    //this.fen = this.boardManager.getFEN();
    //ottengo il PGN della partita
    this.pgn = this.board.getPGN();
    var temp = this.pgn.split(' ');
    // Se temp.length - 2 termina con un punto, e' una mossa del bianco, altrimenti del nero
    // es: 1. e4 e5
    if (temp[temp.length-2].endsWith('.')) {
      //this.whiteMoves.push(temp[temp.length-1]);
      this.whiteToMove = false;
    } else {
      //this.blackMoves.push(temp[temp.length-1]);
      this.whiteToMove = true;
    }
}
  

  public nextMove(): void {
    let moveDone = false;
    if (this.move <this.game.moves.length) {
      if (this.whiteToMove) {
        console.log("sono dentro a white")
        this.temppgn = this.temppgn + (this.turn+1).toString() + '. ' + this.game.moves[this.move].move + ' ';
        console.log(this.temppgn);
        this.whiteToMove = false;
        this.board.setPGN(this.temppgn) 
        moveDone = true;    
      }
      else {
        console.log("sono dentro a black")
        this.temppgn = this.temppgn + this.game.moves[this.move].move + ' ';
        console.log(this.temppgn);
        this.whiteToMove = true;
        this.board.setPGN(this.temppgn);
        this.turn++;
        moveDone = true;
      }
    }
        //if (this.whiteToMove) {
    //  if (this.turn <= this.trueturn) { 
    //  this.temppgn = this.temppgn + (this.turn + 1).toString() + '. ' + this.whiteMoves[this.turn] + ' ';
    //  this.whiteToMove = false;
      //console.log(this.temppgn)
    //  this.board.setPGN(this.temppgn);
    //  moveDone = true;
    //  }
    //} else {
    //  if (this.turn*2 <= this.trueturn-2){
        //console.log('sono dentro a black moves')
        //console.log(this.turn*2);
        //console.log(this.trueturn);
    //    this.temppgn = this.temppgn + this.blackMoves[this.turn] + ' ';
    //    this.whiteToMove = true;
    //    this.board.setPGN(this.temppgn);
    //    this.turn++; moveDone = true;
    //  }
    //}
    if (moveDone == true) {
    //messo alla veloce
      this.move++;
      this.commentCount++;
    }
  }

  public prevMove(): void {
    // se white to Move è true, ha appena mosso il nero
      if (this.turn >= 0) {
        if (this.whiteToMove && this.temppgn != '') {
          if (this.turn > 0) {
            this.turn--;
          }
          //console.log(this.temppgn);
          //console.log(this.blackMoves[this.turn])
          this.temppgn = this.temppgn.substring(0, this.temppgn.lastIndexOf(this.blackMoves[this.turn]))
          this.whiteToMove = false;
          console.log(this.temppgn);
          this.board.setPGN(this.temppgn);
          this.move--;
          this.commentCount--;
          //console.log(this.move);
        } else {
      // devo rimuovere l'ultima mossa del bianco, ma questa è formata sia dalla mossa, sia dall'indicazione del turno
        if (this.turn == 0) {
          //console.log('sono dentro a beginning')
          this.beginning();
          this.whiteToMove = true;
        } else {
          this.temppgn = this.temppgn.substring(0, this.temppgn.lastIndexOf((this.turn + 1).toString() + '.'));
          this.whiteToMove = true;
          //console.log(this.temppgn);
          this.board.setPGN(this.temppgn);
          this.move--;
          this.commentCount--;
          //console.log(this.move);
      }
    }
  }
}

counter() {
	if(this.whiteMoves.length > this.blackMoves.length){

		return new Array(this.whiteMoves.length) ;
	}else{
		return new Array(this.blackMoves.length);
	}

}

  public beginning(): void {
    this.temppgn = '';
    this.board.reset();
    this.turn = 0;
    this.move = 0;
    this.commentCount = -1;
    this.whiteToMove = true;
    console.log(this.move);
  }

//Tutoring
closeResult: string= "";
tutors: Tutor[];
tutorsUsername: Array<string> = [];
loadingModel:boolean = false;
@ViewChild('inviteModal') inviteModal : any;

  openModalInvite(){
    this.openTutoring();
    this.modalService.open(this.inviteModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
    });
  }

  openTutoring(){
    this.loadingModel = true;
    this.accountService.account()
        .subscribe({
          next: (resp) => {
            this.tutors = resp;
            this.tutors.forEach(t => {
              if(this.user?.username != t.username)
                this.tutorsUsername.push(t.username);
            });
          },
          error: error => {
            this.close();
            this.loadingModel = false;
            this.alertService.error(error.error);
          }
        });

      this.loadingModel = false;
  }

  


  close(){
    this.modalService.dismissAll();
  }

  invite(){
      var tut:Tutor = new Tutor();
      this.tutors.forEach(t => {
        if(t.username == this.model){
            tut = t;
        }
      });
      let tutoring = new Tutoring();
      tutoring.gameId = this.id;
      tutoring.tutorId = tut.id;
      tutoring.userId = this.userid;

      if(tut.username == ""){
        this.close();
        this.alertService.error("Errore nella selezione dell'utente!");

      }else{
        this.tutorService.set(tutoring)
        .subscribe({
          next: () => {
            this.close();
            this.alertService.success("Tutor invitato correttamente!");
          },
          error: error => {
            this.close();
            this.alertService.error("Errore durante l'invito del tutor!");
          }
        });
      }
  }

  /* 
  *   Gestione Modal Generico
  */
  
  open(content:any) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
    
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  formatter = (result: string) => result.toUpperCase();

  public model: any;

  search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term === '' ? []
        : this.tutorsUsername.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

}

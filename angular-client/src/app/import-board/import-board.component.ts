import { SplitInterpolation } from '@angular/compiler';
import { ViewChild, ChangeDetectionStrategy, Component, EventEmitter, Output, OnInit } from '@angular/core';
import {
  MoveChange,
  NgxChessBoardComponent,
  NgxChessBoardService,
  NgxChessBoardView,
  PieceIconInput
} from 'ngx-chess-board'
import { BehaviorSubject, first } from 'rxjs';
import { faBan, faForwardStep, faBackwardStep, faChessPawn, faCircleNotch, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomBoardService } from '../_services/custom-board.service';
import { AccountService } from '../_services';
import { AlertService } from '../_services/alert.service';
import { FuncsService } from '../_services/funcs.services';
import { Router } from '@angular/router';
import { User } from '../_models';
import { CustomChessboard } from '../_models/custom-board';
import { Move, Partita } from '../_models/partita';
import { ChessboardService } from '../_services/chessboard.service';

@Component({
  selector: 'import-board',
  templateUrl: './import-board.component.html',
  styleUrls: ['./import-board.component.css'],
  //changeDetection: ChangeDetectionStrategy.OnPush, ????
})
export class ImportBoardComponent implements OnInit {
  @ViewChild('board', {static: false}) board: NgxChessBoardComponent;
  faBan = faBan;
  faForwardStep = faForwardStep;
  faBackwardStep = faBackwardStep;
  faChessPawn = faChessPawn;
  faCircleNotch= faCircleNotch;
  faChevronLeft = faChevronLeft;
  isWhite:boolean = true;
  closeResult: string= "";
  pgnSetted: boolean= false;
  @ViewChild('resetModal') resetModal : any;
  pieceIconInput: PieceIconInput;
  loading: boolean = false;
  user:User | null = null;
  customBoard: CustomChessboard = new CustomChessboard();
  partita:Partita;


  constructor(
    private ngxChessBoardService: NgxChessBoardService, 
    private config: NgbModalConfig,
    private modalService: NgbModal, 
    private customBoardService: CustomBoardService,
    private accountService: AccountService,
    private chessboardService: ChessboardService,
    private alertService: AlertService,
    private funcs: FuncsService,
    private router: Router
    ) {  }

  public pgn: string = '';
  public temppgn: string = '';
  public whiteMoves: string [] = [];
  public blackMoves: string [] = [];
  public whiteToMove: boolean = true;
  public turn = 0;
  public trueturn = 0;
  public move = 0;
  public comments: any[] = [];
  public lastComment: string = '';
  public namePartita: string = '';
  public visComment : string = '';
  public descrPartita: string = '';


  public saveComment() {
    console.log(this.lastComment);
    var temp = [this.move, this.lastComment];
    var isInComments = false;
    // e' da fixare, per ora salva e basta, non modifica
    for (let i = 0; i < this.comments.length; i++) {
      if (this.move == this.comments[i][0]) {
        isInComments = true;
      }
    }
    if (isInComments == false) {
      this.comments.push(temp);
    }
    for (let i = 0; i < this.comments.length; i++) {
      console.log(this.comments[i]);
    }
    this.setVisComment();
  }

  public createPartita() {
    let savedGame = new Partita();
    // genero i campi della partita obbligatori
    this.accountService.user.subscribe(x => this.user = x); // prendo l'id dell'utente
    if (this.user != null)
      savedGame.id_user = this.user?.id;
    // da fixare, e' solo per prova
    savedGame.name = this.namePartita;
    savedGame.description = this.descrPartita;
    let date = new Date;
    savedGame.date = date.toLocaleDateString();
    // Le mosse provo a darle come un array di stringhe
    // ora vado a riempire l'array di mosse
    //let gameMoves = [];
    //let newMove = new Move();
    //newMove.color = "bianco";
    //newMove.move = "e4";
    //newMove.number = "1";
    //for (let i = 0; i < this.move; i++) {
    //gameMoves.push(newMove);
    //} 

    return savedGame;
  }

  public createArrayPartita() {
    let returnArray = [];
    let returnStr = "";
    for (let i = 0; i < this.blackMoves.length; i++) {
      returnStr = i+1 + ". " + this.whiteMoves[i] + " " + this.blackMoves[i] + " ";
      returnArray.push(returnStr);
    }
    // controlliamo se devo aggiungere una mossa aggiuntiva al bianco
      if (this.whiteMoves.length > this.blackMoves.length) {
      returnStr = this.whiteMoves.length + ". " + this.whiteMoves[this.whiteMoves.length-1];
      returnArray.push(returnStr);
      console.log(returnStr);
    }
    return returnArray;
  }

  // da mettere non utilizzabile appena viene fatta partire la schermata
  public saveGame() {
    let savedGame = this.createPartita();
    let arrayMoves = this.createArrayPartita();
    console.log(arrayMoves);
    if (this.namePartita != '') {
    this.chessboardService.create(savedGame, arrayMoves)
    .subscribe({
      next: (resp) => {
        this.partita = resp;
        this.loading = false;
        this.router.navigate(['/history/'+ this.partita._id.$oid]);
        this.alertService.success("Salvataggio avvenuto correttamente!");
        
      },
      error: error => {
        this.alertService.error("Errore durante il salvataggio!");
        this.loading = false;
      }
    });
  } else {
    this.alertService.error("nome della partita non inserito!");
    this.loading = false;
  }
  }

  public reverse(): void {
    this.isWhite = !this.isWhite;
    this.board.reverse();
  }
  
  public moveCallback(move: MoveChange): void {
        //this.fen = this.boardManager.getFEN();
        //ottengo il PGN della partita
        this.pgn = this.board.getPGN();
        var temp = this.pgn.split(' ');
        // Se temp.length - 2 termina con un punto, e' una mossa del bianco, altrimenti del nero
        // es: 1. e4 e5
        if (temp[temp.length-2].endsWith('.')) {
          //this.whiteMoves.push(temp[temp.length-1]);
          this.whiteToMove = false;
        } else {
          //this.blackMoves.push(temp[temp.length-1]);
          this.whiteToMove = true;
        }
        console.log(move);
    }

  public setPgn() {
        var tempInd = this.pgn.lastIndexOf(']');
        var temp2 = this.pgn.substring(tempInd+1);
        temp2 = temp2.replace(/(\r\n|\n|\r)/gm, ' ');
        console.log('temp2: ', temp2);

        var temp = temp2.trim().split(' ');
        // 0 per l'indice mossa, 1 per la mossa del bianco e 2 per quella del nero
        // l'ultimo elemento e' lo score a fine partita
        for (let i = 0; i < temp.length-1; i++) {

          if (!temp[i].endsWith('.')){
            if (this.whiteToMove == true) {
              console.log(temp[i])
              this.whiteMoves.push(temp[i]);
              this.whiteToMove = false;
            } else {
              this.blackMoves.push(temp[i]);
              this.whiteToMove = true;
            }
        }
      }
      this.trueturn = ((temp.length / 3) * 2) - 1;
      // se l'ultimo elemento non e' uno score, lo attacco all'array corretto
      //if (temp[length])
      this.whiteToMove = true;

      this.pgnSetted = true;
        //this.board.setPGN(this.pgn);
    
  }

  public setVisComment(): void {
    var commExists = false;
    var whatMove = 0;
    for (let i = 0; i < this.comments.length; i++) {
      if (this.move == this.comments[i][0]) {
        commExists = true;
        whatMove = i;
      }
    }
    if (commExists == true) {
      this.visComment = this.comments[whatMove][1];
    } else {
      this.visComment = '';
    }
    console.log(this.visComment);
  }

  public nextMove(): void {
    if (this.whiteToMove) {
      if (this.turn < this.trueturn) { 
      this.temppgn = this.temppgn + (this.turn + 1).toString() + '. ' + this.whiteMoves[this.turn] + ' ';
      this.whiteToMove = false;
      //console.log(this.temppgn)
      this.board.setPGN(this.temppgn);
      }
    } else {
      if (this.turn*2 < this.trueturn-2){
        console.log('sono dentro a black moves')
        console.log(this.turn*2);
        console.log(this.trueturn);
        this.temppgn = this.temppgn + this.blackMoves[this.turn] + ' ';
        this.whiteToMove = true;
        this.board.setPGN(this.temppgn);
        this.turn++;
      }
    }
    //messo alla veloce
    this.move++;
    this.setVisComment();
  }

  //devo aggiungere il move--
  public prevMove(): void {
    // se white to Move è true, ha appena mosso il nero
      if (this.turn >= 0) {
        if (this.whiteToMove && this.temppgn != '') {
          if (this.turn > 0) {
            this.turn--;
          }
          console.log(this.temppgn);
          console.log(this.blackMoves[this.turn])
          this.temppgn = this.temppgn.substring(0, this.temppgn.lastIndexOf(this.blackMoves[this.turn]))
          this.whiteToMove = false;
          console.log(this.temppgn);
          this.board.setPGN(this.temppgn);
          this.move--;
          console.log(this.move);
        } else {
      // devo rimuovere l'ultima mossa del bianco, ma questa è formata sia dalla mossa, sia dall'indicazione del turno
        if (this.turn == 0) {
          console.log('sono dentro a beginning')
          this.beginning();
          this.whiteToMove = true;
        } else {
          this.temppgn = this.temppgn.substring(0, this.temppgn.lastIndexOf((this.turn + 1).toString() + '.'));
          this.whiteToMove = true;
          console.log(this.temppgn);
          this.board.setPGN(this.temppgn);
          this.move--;
          console.log(this.move);
      }
    }
  }
  this.setVisComment();
}



counter() {
	if(this.whiteMoves.length > this.blackMoves.length){

		return new Array(this.whiteMoves.length) ;
	}else{
		return new Array(this.blackMoves.length);
	}

}
  
  public beginning(): void {
    this.temppgn = '';
    this.board.reset();
    this.turn = 0;
    this.move = 0;
    console.log(this.move);
  }
  // go to end da implementare

  public undo(): void {
        this.board.undo();
    }


  openModalReset(){
    this.modalService.open(this.resetModal, {ariaLabelledBy: 'modal-basic-title',backdrop : 'static', keyboard : false}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;               
    });
}

  reset(): void {
    this.board.reset();
    this.whiteMoves = [];
    this.blackMoves = [];
    this.turn = 0;
    this.trueturn = 0;
    this.move = 0;
    this.pgn = '';
    this.temppgn = '';
    this.comments = [];
    this.pgnSetted = false;
    this.modalService.dismissAll();
    this.setVisComment();
}

closeReset(){
    this.modalService.dismissAll();

}



  /* 
  *   Gestione Modal Generico
  */

  open(content:any) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit(): void {
    this.loading = true;
    this.accountService.user.subscribe(x => this.user = x);
    let id = "";

    if(this.user != null){
      id = this.user.id;
    
      this.customBoardService.get(id)
      .pipe(first())
      .subscribe({
        next: (resp) => {
          this.customBoard = resp;
          this.customBoard.userId = id
        },
        error: error => {
          this.router.navigate(["/"]);
          this.alertService.error(error.error);
        }
      });

      this.config.backdrop = 'static';
      this.config.keyboard = false;
    
      this.pieceIconInput =this.funcs.getPieceIconInput();
      this.loading = false;

    }else{
       this.router.navigate(["/"]);
    }

  }

  back(){
    this.router.navigate(['/']);  
  }
  
  @Output() public restart = new EventEmitter<void>();
}

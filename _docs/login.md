# Login
## Come funziona?

Il client in angular comunica attraverso delle chiamate API REST con il backend in asp.net 5, il tutto viene salvato in un Database di tipo non relazione, MongoDB, utilizzando il pacchetto MongoDB Identity (Per maggiori informazioni https://github.com/matteofabbri/AspNetCore.Identity.Mongo). 
Sono state implementate 2 API REST:
- Creazione di un nuovo profilo
- Login nell'applicazione

La prima permette all'utente di creare un nuovo account, vengono richiesti una email, un nome utente e una password (con dei requisiti, come "almeno 6 caratteri", "un carattere alfanumerico", ...).

La seconda invece permette di accedere all'applicazione, una volta che il backend ha verificato che il l'utente e la password siano corretti, crea un token personale che restituisce al client e viene salvato nella "local session".

## Che tipo di autenticazione abbiamo usato?

Abbiamo utilizzato JWT. JSON Web Token (JWT) è un open standard (RFC 7519) che definisce un modo compatto e autonomo per la trasmissione sicura di informazioni tra le parti come un oggetto JSON. Queste informazioni possono essere verificate e affidabili perché sono firmate digitalmente. Abbiamo firmato i nostri JWT con una chiave privata simmetrica. 

## Dove viene definito nel software?

### Backend (AuthenticationService)

Nel servizio backend in ASP.NET 5.0 in `Startup.cs` nella funzione `ConfigureServices` aggiungiamo l'estensione per la gestione dell'autenticazione JWT:

    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = tru
            ValidIssuer = "https://localhost:5001",
            ValidAudience = "https://localhost:5001",
            IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("PrivateKey"))
        };
    });

Una volta verificato che il `nome utente` la `password` siano corretti aggiungiamo la creazione del token che restituiremo al client:

    var tokenOptions = new JwtSecurityToken(
        issuer: "https://localhost:5001",
        audience: "https://localhost:5001",
        claims: new List<Claim>(),
        expires: DateTime.Now.AddMinutes(5),
        signingCredentials: signingCredentials

    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

### Frontend (Angular-client)

A livello di frontend abbiamo creato due componenti una per il login e uno per la registrazione di un nuovo account. 
Per inserire il token ogni volta che effettuiamo una chiamata al dominio `localhost:5001`, importiamo il modulo JWT. 

    @NgModule({
      imports: [
        [...],
        JwtModule.forRoot({
          config: {
            tokenGetter: tokenGetter,
            allowedDomains: ["localhost:5001"],
            disallowedRoutes: []
          }
        })
      ],
      [...]
    })



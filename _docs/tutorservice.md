# Tutor Service
## Come funziona?
Questa funzionalità è legata a quella di tutoring, quando l'utente decide di analizzare una partita può richiedere sulla partita stessa o su una particolare mossa la supervisone di un utente più esperto, ossia il tutor.
Tale meccanismo avviene tramite una notifica che il tutor riceve a seguito di una richiesta dell'utente.
Questa meccanismo è gestito dal tutor service.

## Che funzionalità vengono date all'utente?
All'utente vengono date le seguenti possibilità:
- scelta del tutor
- possibilità di inserire commento sulla mossa
- invia notifica al tutor 

## Che funzionalità vengono date al tutor?
All'utente vengono date le seguenti possibilità:
- push della notifica ricevuta
- possibilità di inserire commento sulla mossa
- possibilità di smettere di seguire una partita come tutor


## Dove viene definito nel software?
### Backend (tutor-service)

Se si apre il microservizio si può trovare la cartella Models che contiene la definizione della classe Tutor, mentre nella cartella Service la classe TutorService implementa i metodi per la creazione, modifica o rimozione di un Tutor e gestisce la comunicazione con il Database.

### Frontend (Angular-client)

Se si apre il progetto in angular, dentro la certella "service" si può trovare:

        tutor.service.ts
		
In particolare le operazioni get e set permettono rispettivamente di richiedere un nuovo tutor e di modificare un tutor.





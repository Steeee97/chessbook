# Chessboard Service
## Come funziona?

Il client in angular comunica attraverso delle chiamate API REST con il microservizio (chessboard-serive) in python, il tutto viene salvato in un Database di tipo non relazione, MongoDB. 

Sono state implementate Sono state implementate 4 API REST:
- POST, permette di creare una partita e un commento
- GET, permette di prendere una partita specifica (ricerca tramite id) e utte le partite di un utente (storico partite e storico commenti)
- PUT, permette di modificare un commento
- DELETE, permette di eliminare una partita

## Dove viene definito nel software?

### Backend (chessboard-service)

Se si apre il microservizio si può trovare l'application Chessboard.py che contiene la definizione delle classi: partita, move, comment che permettono la creazione degli oggetti per l'utilizzo ncessario.

In python viene utilizzato il micro-framework Flask che permette la definizione:
- API REST con la quale comunica con il front-end nel formato JSON 
- Connessione DB, dove sono definite le impostazioni per la comunicazione con il DB.

Sono state implementate le seguenti API REST:
- @service.route('/match', methods=['POST']), permette la creazione di una partita normalizzando i dati in modo tale da definire una struttura ben precisa per il salavataggio nel DB.
- @service.route('/match', methods=['POST']), permette la creazione/ l'aggiunta di un commento rispetto a una determinata partita e mossa
- @service.route('/match/<id>', methods=["DELETE"]), permete la eliminazione di una partita specifica
- @service.route('/matches/<id_user>', methods=['GET']), permette di prendere lo storico partite di un determinato utente (assieme ai suoi commenti)
- @service.route('/match/<id>', methods=["GET"]), permette di prendere una partita specifica
- @service.route('/match/<id>', methods=['PUT']), permette la modifica di un commento ben specifico

### Frontend (Angular-client)
Se si apre il progetto in Angular, è possibile trovare nella cartella service il file

       Chessboard.service.ts

Questo file contiene la definizione dei servizi API REST. I servizi API rest presenti in questo file permettono di interagire con il database tramite GET, POST e DELETE:
- Le funzioni di GET vengono utilizzate per ottenere le informazioni di una partita; la get in particolar modo viene utilizzata per ottenere tutte le partite di un utente (viene usata, ad esempio, nella history-games.component per ottenere tutte le partite di un utente):

sempio, nella ngOnInit: 

    [...]
    get() {
        [...]
        this.chessboardService.get(id)
            [...]

Non è stata fatta la get di un singolo commento, in quanto i commenti vengono richiamati tutti caricando una partita.
- E’ stato invece creato un metodo post per creare i commenti, oltre alla post per creare una partita, dopo aver importato il PGN tramite la import-board.component. È stato scelto di utilizzare una POST invece di una PUT per i commenti in quanto, avendo nella fase iniziale del progetto l’idea di una “chat” tra l’utente e il tutor, pensavamo che fosse preferibile continuare ad aggiungere commenti, in modo da dare il “feeling” di una chat tra due utenti.
- Infine, abbiamo implementato la DELETE in modo da cancellare una partita specifica. Anche questo metodo viene richiamato all’interno della history.games.component, componente utilizzata per controllare e gestire le partite di un singolo utente.

# Import board
## Come funziona?
Una volta che l'utente ha effettuato il login correttamente, vengono fornite ad esso diverse opzioni nella homepage, definite da diverse componenti tramite Angular. Una di esse da la possibilità di importare una partita tramite un file PGN. Dopo aver effettuato ciò, all'utente vengono date all'utente diverse funzionalità per interagire con la partita, come la possibilità di vedere tutte le mosse della partita, partendo dall'inizio della stessa fino ad arrivare alla conclusione. 
Attualmente ciò viene effuato solo sul lato front end: il motivo per questa scelta è che attualmente le partite che vengono importate dall'utente non vengono salvate sul database, ma solamente in locale (effettuando il refresh della pagina la partita importata viene attualemente persa). Futuri aggiornamenti dell'applicazione permetteranno di salvare la partita su un database non relazionale, MongoDB.

## Cosa sono i file PGN?
La denominazione PGN (Portable Game Notation) è un formato digitale usato in ambito digitale per registrare le partite a scacchi.  Pensato per essere facilmente leggibile dall'uomo e comodamente usato dai software per computer. Le mosse sono descritte in notazione algebrica e solitamente sono raccolte all'interno di file con estensione .pgn.
In un file PGN, dopo i metadati della partita, sono presenti le mosse della stessa, scritte in notazione algebrica. Attualmente è solo supportata la notazione algebrica con il nome dei pezzi in inglese (es: K per re, non R); questa scelta è stata effettuata per mantenere gli standard presenti su altri siti per giocare a scacchi.
## Che funzionalità vengono date all'utente?
All'utente vengono date le seguenti possibilità:
- Frecce direzionali: Queste vengono utilizzate per andare avanti e indietro con le mosse della partita
- Reset: Riporta la scacchiera alla posizione iniziale, la partita importata tramite PGN viene cancellata per fare spazio ad un nuovo import
- Reverse: Permette di invertire la scacchiera, in modo da poter vedere il punto di vista del bianco o del nero
- Import PGN: permette di importare il PGN di una partita copiandolo da un'altra fonte

## Dove viene definito nel software?
All'interno del software questa funzionalità viene definita tramite una componente dedicata, definita import board. In essa, viene utilizzata una libreria esterna per caricare una scacchiera; i metodi presenti all'interno della libreria sono un ibrido tra i metodi già presenti nella libreria esterna e metodi ad-hoc volti ad aggiungere funzionalità (come, per esempio, la possibilità di vedere tutte le mosse di una partita, e non la sua sola posizione finale)

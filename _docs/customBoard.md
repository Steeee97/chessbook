# Personalizza la tua scacchiera
## Come funziona?

Il client in angular comunica attraverso delle chiamate API REST con il microservizio (customBoard-service) in asp.net 5, il tutto viene salvato in un Database di tipo non relazione, MongoDB. 
Sono state implementate 2 API REST:
- Set, permette di creare o modificare la personalizzazione della scacchiera.
- Get, permette di prendere la personalizzazione dell'utente dal DB.

## Dove viene definito nel software?

### Backend (customBoard-service)

Se si apre il microservizio si può trovare la cartella Model che contiene la definizione della classe CustomChessboard, nella cartella Controllers, il controller che gestisce le route per le varie api, la cartella Services la classe CustomChessboardService dove vengono definiti i metodi per leggere e scrivere nel db ed infine la cartella Settings dove sono definite le impostazioni per la comunicazione con il DB.


### Frontend (Angular-client)

Se si apre il progetto in angular, dentro la certella "service" si può trovare:

        custom-board.service.ts

Questa file contiene la definizione dei servizi API REST. Ogni volta che si apre una pagina con la scacchiera viene fatta l'operazione di GET per prendere, se presente la personalizzazione dell'utente. Nel caso la chiamata rispondesse con un valore nullo allora viene messa la configuarazione di default.
Esempio, nella ngOnInit: 

    [...]
    export class ChessboardComponent implements OnInit {
        [...]
        ngOnInit(): void {
            [...]
            
            this.customBoardService.get(id)

            [...]



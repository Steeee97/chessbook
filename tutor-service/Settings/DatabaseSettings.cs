﻿using System;

namespace tutor_service.Settings
{

    public class DatabaseSettings : IDatabaseSettings
    {
        public string TutorCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string TutorCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using tutor_service.Model;
using tutor_service.Settings;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace tutor_service.Services
{
    public class TutorService
    {
        public IMongoCollection<Tutor> _tutorService;

        private readonly IConfiguration _config;

        public TutorService(IConfiguration config)
        {
            _config = config;
            var client = new MongoClient(_config.GetValue<string>("MONGO_CONNECTION"));
            var database = client.GetDatabase(_config.GetValue<string>("MONGO_DATABASE_NAME"));

            _tutorService = database.GetCollection<Tutor>("TutorDB");
        }

        public List<Tutor> Get() =>
            _tutorService.Find(tutor => true).ToList();

        public List<Tutor> Get(string id) =>
            _tutorService.Find(tutor => tutor.TutorId == id).ToList();

        public Tutor Create(Tutor tutor)
        {
            _tutorService.InsertOne(tutor);
            return tutor;
        }

        public void Update(string id, Tutor tutorIn) =>
            _tutorService.ReplaceOne(tutor => tutor.Id == id, tutorIn);

        public void Remove(Tutor tutorIn) =>
            _tutorService.DeleteOne(tutor => tutor.Id == tutorIn.Id);

        public void Remove(string id) =>
            _tutorService.DeleteOne(tutor => tutor.GameId == id);
    }
}

﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tutor_service.Model
{
    public class Tutor
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        public string UserId { get; set; }
        public string GameId { get; set; }
        public string TutorId { get; set; }

        public Tutor()
        {
        }
    }
}

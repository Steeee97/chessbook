﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tutor_service.Model;
using tutor_service.Services;
using Microsoft.AspNetCore.Mvc;

namespace tutor_service.Controllers
{
    [Route("api/tutor")]
    public class TutorController : ControllerBase
    {
        private readonly TutorService _tutorService;

        public TutorController(TutorService tutorService)
        {
            _tutorService = tutorService;
        }

        [HttpGet, Route("test")]
        public ActionResult<List<Tutor>> test()
        {
            return Ok("test");
        }


        [HttpGet]
        public ActionResult<List<Tutor>> Get() =>
            _tutorService.Get();

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            List<Tutor> tutor = _tutorService.Get(id);

            if (tutor == null)
            {
                return Ok(null);
            }

            return Ok(tutor);
        }

        [HttpPost, Route("create")]
        public ActionResult<Tutor> Set([FromBody] Tutor tutor)
        {
            if(tutor.Id != "")
            {
                _tutorService.Update(tutor.Id, tutor);
            }
            else
            {
                _tutorService.Create(tutor);
            }

            return Ok(tutor);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, Tutor tutorIn)
        {
            var tutorService = _tutorService.Get(id);

            if (tutorService == null)
            {
                return NotFound();
            }

            _tutorService.Update(id, tutorIn);

            return Ok(tutorIn);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var tutorService = _tutorService.Get(id);

            if (tutorService == null)
            {
                return NotFound();
            }

            _tutorService.Remove(id);

            return Ok();
        }
    }

}


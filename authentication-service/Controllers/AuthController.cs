﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using AuthenticationService.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AuthenticationService.Controllers
{
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private UserManager<ApplicationUser> userManager;
        private SignInManager<ApplicationUser> signInManager;

        public AuthController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpPost, Route("login")]
        public async Task<IActionResult> LoginAsync([FromBody]LoginModel user)
        {
            if (user == null)
                return BadRequest("Invalid client request");

            ApplicationUser appUser = await userManager.FindByNameAsync(user.Username);
            if (appUser != null)
            {
                Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.PasswordSignInAsync(appUser, user.Password, false, false);
                if (result.Succeeded)
                {

                    var secretKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("superSecretKey@345"));
                    var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                    /*
                    var claims = new List<Claim>
                    {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Role, "Admin")
                    };*/

                    var tokenOptions = new JwtSecurityToken(
                        issuer: "http://localhost:5001",
                        audience: "http://localhost:5001",
                        claims: new List<Claim>(),
                        expires: DateTime.Now.AddMinutes(600),
                        signingCredentials: signingCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

                    User usr = new User();
                    usr.Id = appUser.Id.ToString();
                    usr.Username = appUser.UserName;
                    usr.Email = appUser.Email;
                    usr.Token = tokenString;

                    return Ok(usr);

                }
            }
            

            //Call al db
            /*
            if (user.Username == "admin" && user.Password == "admin")
            {
                var secretKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Role, "Admin")
                };

                var tokenOptions = new JwtSecurityToken(
                    issuer: "https://localhost:5001",
                    audience: "https://localhost:5001",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: signingCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

                User usr = new User();
                usr.Username = "Admin";
                usr.Token = tokenString;

                return Ok(usr);
            }*/

            return Unauthorized();
        }



        [HttpPost, Route("register")]
        public async Task<IActionResult> Create([FromBody] User user)
        {

            ApplicationUser appUser = new ApplicationUser
            {
                UserName = user.Username,
                Email = user.Email
            };

            IdentityResult result = await userManager.CreateAsync(appUser, user.Password);
            if (result.Succeeded)
                return Ok();
            else
            {
                String[] Error = new String[5];
                int i = 0;
                foreach (IdentityError error in result.Errors)
                {
                    if (i < Error.Length)
                        Error[i] = error.Description;
                    i++;
                }
                return BadRequest(Error);

            }
        }

    }
}

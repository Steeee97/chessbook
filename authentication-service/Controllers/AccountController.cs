﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AuthenticationService.Controllers
{
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private UserManager<ApplicationUser> userManager;

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }


        [HttpGet]
        public IActionResult allUser()
        {
            List<ApplicationUser> user = userManager.Users.ToList();
            List<Tutor> tutors = new List<Tutor>();
            Tutor temp;
            foreach(ApplicationUser u in user)
            {
                temp = new Tutor();
                temp.Id = u.Id.ToString();
                temp.Username = u.UserName;

                tutors.Add(temp);
            }

            return Ok(tutors);
        }


    }
}

﻿using System.ComponentModel.DataAnnotations;
using System;
namespace AuthenticationService.Model
{
    public class Tutor
    {
        public string Id { get; set; }
        [Required]
        public string Username { get; set; }

        public Tutor()
        {
            Id = "0";
            Username = "";
        }
    }
}

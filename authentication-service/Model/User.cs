﻿using System.ComponentModel.DataAnnotations;
using System;
namespace AuthenticationService.Model
{
    public class User
    {
        public string Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        public Role Role { get; set; }
        [Required]
        public string Password { get; set; }
        public string Token { get; set; }

        public User()
        {
            Id = "0";
            Username = "";
            Role = Role.User;
            Password = "";
            Token = "";

        }
    }

    public enum Role
    {
        Admin,
        User
    }
}

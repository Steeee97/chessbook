﻿using System;
namespace AuthenticationService.Settings
{

    public class DatabaseSettings : IDatabaseSettings
    {
        public string CustomChessboardCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string CustomChessboardCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

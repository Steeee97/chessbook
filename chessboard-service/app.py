#pip install Flask
#pip install Flask-PyMongo

from flask import Flask, Response, jsonify, request
from flask_pymongo import PyMongo
from bson import json_util
from bson.objectid import ObjectId
import itertools
import json
from jinja2.utils import markupsafe 
markupsafe.Markup()
import pymongo
from pymongo import MongoClient
from flask_cors import CORS, cross_origin

#### Avvio servizio [start] + connessione mongodb
service = Flask(__name__)
cors = CORS(service)
service.config['CORS_HEADERS'] = 'Content-Type'


# Configurazione d'accesso a mongo
# Crea il db se non esiste
#service.config['MONGO_URI'] = 'mongodb://chessboardservicedb:27019/chessbook'  # Non funziona in locale!!!!!!! sostituire chessboard.. con localhost
#mongo = PyMongo(service)

def get_db():
    client = MongoClient(host='chessboardservicedb',
                         port=27017, 
                         username='root', 
                         password='pass',
                         authSource="admin")
    db = client["chessbook_db"]
    return db



class Partita:

    def __init__(self, date, id_user, name, description, moves):
        self.date = date
        self.id_user = id_user
        self.name = name
        self.description = description
        self.moves = []
        
        if moves != None :
            for mov in moves:
                mov = mov.replace(".","")
                split = mov.split()
                if len(split) == 3:
                    #add move(number, move, color, comments = 0)
                    self.moves.append(Move(int(split[0]), split[1], 'WHITE'))
                    self.moves.append(Move(int(split[0]), split[2], 'BLACK'))
                elif len(split) == 2:
                    self.moves.append(Move(int(split[0]), split[1], 'WHITE'))
        else:
            print("Error: There aren't moves")     
        
    def insert_moves(self):
        arr_moves = []
        for move in self.moves:
            arr_moves.append(move.insert_move())
        return arr_moves     


class Comment:
    id_iter = itertools.count()

    def __init__(self, id_user, date, comment):
        self.id_comment = next(self.id_iter)
        self.id_user = id_user
        self.date = date
        self.comment = comment

    def json_comment(self):
        return ({'id_comment': self.id_comment, 'id_user': self.id_user, 'date': self.date, 'comment': self.comment})

class Move:
    id_iter = itertools.count()

    def __init__(self, number, move, color, comments = None):
        self.id_move = next(self.id_iter)
        self.number = number
        self.move = move
        self.color = color
        self.comments = []

        if comments != None :
            for c in comments:
                self.comments.append(Comment(c[0], c[1])) #add comment(id_user, string)
    
    # add comment in list and return the json to upload in DB
    def add_comment(self, id_user, comment):
        c = Comment(id_user, comment)
        self.comments.append(c)
        return c.json_comment()

    # def add_comment(self, id_user, comment):
    #     self.comments.append(Comment(id_user, comment))
       
    def insert_move(self):
        move= {
            'id_move': self.id_move,
            'number': self.number,
            'move': self.move,
            'color': self.color,
            'comments': self.comments
        }

        return move

@service.route('/match', methods=['POST'])
def create_match():
    #Receiving data
    print(request.json)
    date = request.json['date']
    id_user = request.json['id_user']
    name_match = request.json['name']
    description = request.json['description']
    moves = request.json['moves']
    match = Partita(date, id_user, name_match, description, moves)

    if date and id_user and name_match and moves:
        #output id.acknowledged (TRUE/FALSE), id.inserted_id (identificativo nel BD)
        db = get_db()
        id = db.matches.insert_one({'date': date, 'id_user': id_user, 'name': name_match, 'description': description}) #crea la collezione se non esiste
        for mov in match.insert_moves():
                db.matches.update_one(
                    {"_id": id.inserted_id},
                    {"$push": {"moves": mov}}
                )
        response = get_match(id.inserted_id)
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response
    else:
        response = jsonify({'a_message': 'There are attributes empty', 'date': date, 'id_user': id_user, 'name': name_match, 'moves': moves})
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response

@service.route('/matches/<id_user>', methods=['GET'])
def get_matches(id_user):
    db = get_db()
    matches = db.matches.find({'id_user': id_user})
    response = json_util.dumps(matches)
    return Response(response, mimetype='application/json')

@service.route('/match/<id>', methods=["GET"])
def get_match(id):
    db = get_db()
    match = db.matches.find_one({'_id': ObjectId(id)})
    if match is not None:
        response = json_util.dumps(match)
        return Response(response, mimetype='application/json')
    return not_found()

@service.route('/', methods=["GET"])
def ping_server():
    return "this works!"


@service.route('/match/<id>', methods=['POST'])
@cross_origin()
def create_comment(id):
    db = get_db()
    # id = id partita
    id_move = request.json['id_move']
    id_user = request.json['id_user']
    date_comment = request.json['date']
    comment = request.json['comment']
    print(id_move, id_user, comment)
    if id_user and comment:
        db.matches.update_one(
            {'_id': ObjectId(id), "moves.id_move": id_move},
            {"$push": {"moves.$.comments": Comment(id_user, date_comment, comment).json_comment()}}
        )
        response = jsonify({'message': 'Comment "' + comment + '" was created successfully'})
        return response
    else:
        return jsonify({'message': 'There are attributes empty'})

#da concludere
@service.route('/match/<id>', methods=['PUT'])
@cross_origin()
def update_comment(id):
    db = get_db()
    # id = id partita
    id_move = request.json['id_move']
    id_user = request.json['id_user']
    id_comment = int(request.json['id_comment'])
    new_comment = request.json['comment']

    print(id_move, id_user, id_comment, new_comment)
    if id_user:
        match = db.matches.find_one({'_id': ObjectId(id)})
        document = json_util.dumps(match)
        print("prima ricerca\n",document)

        match2 = db.matches.find_one({'_id': ObjectId(id), 'moves.id_move': id_move})
        document2 = json_util.dumps(match2)
        print("seconda ricerca \n",document2)

        match3 = db.matches.find_one({'_id': ObjectId(id), 'moves.id_move': id_move, "moves.comments.$": [{'id_comment': id_comment}]})
        document3 = json_util.dumps(match3)
        print("terza ricerca\n", document3)
        
        return "funziona"
    else:
        return jsonify({'message': 'There are attributes empty'})

@service.route('/match/<id>', methods=["DELETE"])
def delete_match(id):
    db = get_db()
    match = db.matches.delete_one({'_id': ObjectId(id)})
    print(match)
    if match:
        return jsonify({'message': 'User' + id + ' was Deleted successfully'})
    return not_found()

@service.errorhandler(404)
def not_found(error=None):
    response = jsonify({'message': 'Resource Not Found: '+ request.url, 'status': 404})
    response.status_code = 404
    return response

if __name__ == '__main__':
    service.run(debug=True, port=4000)



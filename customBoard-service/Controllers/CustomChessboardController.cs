﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using customBoard_service.Model;
using customBoard_service.Services;
using Microsoft.AspNetCore.Mvc;

namespace customBoard_service.Controllers
{
    [Route("api/custChessboard")]
    public class CustomChessboardController : ControllerBase
    {
        private readonly CustomChessboardService _custChessboardService;

        public CustomChessboardController(CustomChessboardService custChessboardService)
        {
            _custChessboardService = custChessboardService;
        }

        [HttpGet, Route("test")]
        public ActionResult<List<CustomChessboard>> test()
        {
            return Ok("test");
        }


        [HttpGet]
        public ActionResult<List<CustomChessboard>> Get() =>
            _custChessboardService.Get();

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            CustomChessboard customChessboard = _custChessboardService.Get(id);

            if (customChessboard == null)
            {
                return Ok(null);
            }

            return Ok(customChessboard);
        }

        [HttpPost, Route("create")]
        public ActionResult<CustomChessboard> Set([FromBody] CustomChessboard customChessboard)
        {
            if(customChessboard.Id != "")
            {
                _custChessboardService.Update(customChessboard.Id, customChessboard);
            }
            else
            { 
                _custChessboardService.Create(customChessboard);
            }

            return Ok(customChessboard);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, CustomChessboard customChessboardIn)
        {
            var customChessboard = _custChessboardService.Get(id);

            if (customChessboard == null)
            {
                return NotFound();
            }

            _custChessboardService.Update(id, customChessboardIn);

            return Ok(customChessboardIn);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var customChessboard = _custChessboardService.Get(id);

            if (customChessboard == null)
            {
                return NotFound();
            }

            _custChessboardService.Remove(id);

            return Ok();
        }
    }

}


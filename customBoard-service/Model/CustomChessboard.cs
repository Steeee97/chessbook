﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace customBoard_service.Model
{
    public class CustomChessboard
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        public string UserId { get; set; }
        public string LightColor { get; set; }
        public string DarkColor { get; set; }
        public Boolean ShowCoords { get; set; }
        public Boolean showLastMove { get; set; }
        public Boolean showLegalMoves { get; set; }
        public Boolean showActivePiece { get; set; }
        public string sourcePointColor { get; set; }
        public string destinationPointColor { get; set; }
        public Boolean showPossibleCaptures { get; set; }


        public CustomChessboard()
        {
        }
    }
}

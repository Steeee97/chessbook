﻿using System;
using System.Collections.Generic;
using customBoard_service.Model;
using customBoard_service.Settings;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace customBoard_service.Services
{
    public class CustomChessboardService
    {
        public IMongoCollection<CustomChessboard> _custChessboards;

        private readonly IConfiguration _config;

        public CustomChessboardService(IConfiguration config)
        {
            _config = config;
            var client = new MongoClient(_config.GetValue<string>("MONGO_CONNECTION"));
            var database = client.GetDatabase(_config.GetValue<string>("MONGO_DATABASE_NAME"));

            _custChessboards = database.GetCollection<CustomChessboard>("CustomChessboardDB");
        }

        public List<CustomChessboard> Get() =>
            _custChessboards.Find(customChessboard => true).ToList();

        public CustomChessboard Get(string id) =>
            _custChessboards.Find<CustomChessboard>(customChessboard => customChessboard.UserId == id).FirstOrDefault();

        public CustomChessboard Create(CustomChessboard customChessboard)
        {
            _custChessboards.InsertOne(customChessboard);
            return customChessboard;
        }

        public void Update(string id, CustomChessboard customChessboardIn) =>
            _custChessboards.ReplaceOne(customChessboard => customChessboard.Id == id, customChessboardIn);

        public void Remove(CustomChessboard customChessboardIn) =>
            _custChessboards.DeleteOne(customChessboard => customChessboard.Id == customChessboardIn.Id);

        public void Remove(string id) =>
            _custChessboards.DeleteOne(customChessboard => customChessboard.UserId == id);
    }
}
